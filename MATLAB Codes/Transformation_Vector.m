%MATLAB CODE C-1: Elasticity, Applications and Numerics
%Program to Calculate Components of a Second-Order Tensor (Matrix) under a
%rotation transformation
%Q = Rotational Tensor, A = Original Tensor to be Transformed, AP =
%Transformed Tensor clear
%Input Tensors (matrices) in MATLAB format
Q=[1,0,0;0,0.7071,0.7071;0,-0.7071,0.7071]
B=[1;1;0]
BP= [0;0;0]
%Apply transformation law

acum = 0;
for i= 1:3
    for j=1:3
      acum = acum + Q(i,j)*B(j)
    end   
    BP(i) = acum;
    acum = 0;
end

%AP=Q*A*Q';
%Display Segment
disp('Original Matrix')
disp(B)
disp('Rotation Matrix')
disp(Q)
disp('Transformed Matrix')
disp(BP)