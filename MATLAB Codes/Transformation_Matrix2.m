%MATLAB CODE C-1: Elasticity, Applications and Numerics
%Program to Calculate Components of a Second-Order Tensor (Matrix) under a
%rotation transformation
%Q = Rotational Tensor, A = Original Tensor to be Transformed, AP =
%Transformed Tensor clear
%Input Tensors (matrices) in MATLAB format
Q=[1,0,0;0,0.7071,0.7071;0,-0.7071,0.7071]
A=[1,1,1;1,0,2;0,1,4]
AP= [0,0,0;0,0,0;0,0,0]
%Apply transformation law

acum = 0;
for i= 1:3
   for j=1:3
       
       for p = 1:3
           for q=1:3
               acum = acum + Q(i,p)*Q(j,q)*A(p,q)
            %  AP(i,j)= Q(i,p)*Q(j,q)*A(p,q)
           end   
       end
    AP(i,j) = acum;
    acum = 0;
   end
end

%AP=Q*A*Q';
%Display Segment
disp('Original Matrix')
disp(A)
disp('Rotation Matrix')
disp(Q)
disp('Transformed Matrix')
disp(AP)