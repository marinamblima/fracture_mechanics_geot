%MATLAB CODE C-1: Elasticity, Applications and Numerics
%Program to Calculate Components of a Second-Order Tensor (Matrix) under a
%rotation transformation
%Q = Rotational Tensor, A = Original Tensor to be Transformed, AP =
%Transformed Tensor clear
%Input Tensors (matrices) in MATLAB format
syms a11 a12 a22 a21 a
syms x11 x12 x13 x21 x22 x23 x31 x32 x33 b
Q=[x11,x12,x13;x21,x22,x23;x31,x32,x33]
A=[a,0,0;0,a,0;0,0,a]
AP= [b,b,b;b,b,b;b,b,b]
%Apply transformation law

acum= [b,b,b;b,b,b;b,b,b]
for i= 1:3
   for j=1:3
       
       for p = 1:3
           for q=1:3
               acum(i,j) = acum(i,j) + Q(i,p)*Q(j,q)*A(p,q)
            %  AP(i,j)= Q(i,p)*Q(j,q)*A(p,q)
           end   
       end
    AP(i,j) = acum(i,j);
   end
end

%AP=Q*A*Q';
%Display Segment
disp('Original Matrix')
disp(A)
disp('Rotation Matrix')
disp(Q)
disp('Transformed Matrix')
disp(AP)