%MATLAB CODE C-1: Elasticity, Applications and Numerics
%Program to Calculate Components of a Second-Order Tensor (Matrix) under a
%rotation transformation
%Q = Rotational Tensor, A = Original Tensor to be Transformed, AP =
%Transformed Tensor clear
%Input Tensors (matrices) in MATLAB format
syms cos_theta
syms sin_theta
syms x
Q=[cos_theta,sin_theta;-sin_theta,cos_theta]
e=[0.002,0.001;0.001,-0.004] %strain tensor
AP= [0,0;0,0]
%Apply transformation law

theta = 0.0
angle(1) = 0.0
for n = 1:50
    dt = 3.14/50;
    theta = theta + dt;
    angle(n+1) = theta;
end

   for n=1:50
       e_prime(1,n) = 0.5*(e(1,1)+e(2,2))+0.5*(e(1,1)-e(2,2))*cos(2*angle(n))+e(1,2)*sin(2*angle(n));
       e_prime(2,n) = 0.5*(e(1,1)+e(2,2))-0.5*(e(1,1)-e(2,2))*cos(2*angle(n))-e(1,2)*sin(2*angle(n));
       e_prime(3,n) = 0.5*(e(2,2)-e(1,1))*sin(2*angle(n))+e(1,2)*cos(2*angle(n));
     %  e_prime(2,1,n) = e_prime(1,2,n)
   end

%AP=Q*A*Q';
%Display Segment

plot(n,e_prime(1,n))

%disp('Original Matrix')
%disp(e)
%disp('Rotation Matrix')
%disp(e_prime)
%disp('Transformed Matrix')
%disp(AP)