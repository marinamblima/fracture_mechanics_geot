%MATLAB CODE 3.10: Elasticity, Applications and Numerics
%Considering P =1
x=[-2:0.1:2];
y=[-2:0.1:0];


Tmax = 0.

for i=1:21
    for j=1:41
    Tmax(i,j) = ((pi*(x(j)^2+y(i)^2))^(-1))*(y(i));
    end
end

contour(x,y,Tmax,15)