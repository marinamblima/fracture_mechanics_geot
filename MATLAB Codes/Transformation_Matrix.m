%MATLAB CODE C-1: Elasticity, Applications and Numerics
%Program to Calculate Components of a Second-Order Tensor (Matrix) under a
%rotation transformation
%Q = Rotational Tensor, A = Original Tensor to be Transformed, AP =
%Transformed Tensor clear
%Input Tensors (matrices) in MATLAB format
Q=[-0.7071,0.7071,0;0.7071,0.7071,0;0,0,1]
A=[-1,1,0;1,-1,0;0,0,1]
%Apply transformation law
AP=Q*A*Q';
%Display Segment
disp('Original Matrix')
disp(A)
disp('Rotation Matrix')
disp(Q)
disp('Transformed Matrix')
disp(AP)