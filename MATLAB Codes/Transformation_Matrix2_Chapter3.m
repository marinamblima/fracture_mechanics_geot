%MATLAB CODE C-1: Elasticity, Applications and Numerics
%Program to Calculate Components of a Second-Order Tensor (Matrix) under a
%rotation transformation
%Q = Rotational Tensor, A = Original Tensor to be Transformed, AP =
%Transformed Tensor clear
%Input Tensors (matrices) in MATLAB format

Q=[2/sqrt(6),1/sqrt(6),1/sqrt(6);-1/sqrt(3),1/sqrt(3),1/sqrt(3);0,-1/sqrt(2),1/sqrt(2)]
e=[3,1,1;1,0,2;1,2,0] %original stress tensor
%AP= [0,0;0,0]
%Apply transformation law


   for n=1:50
       e_prime(1,n) = 0.5*(e(1,1)+e(2,2))+0.5*(e(1,1)-e(2,2))*cos(2*angle(n))+e(1,2)*sin(2*angle(n));
       e_prime(2,n) = 0.5*(e(1,1)+e(2,2))-0.5*(e(1,1)-e(2,2))*cos(2*angle(n))-e(1,2)*sin(2*angle(n));
       e_prime(3,n) = 0.5*(e(2,2)-e(1,1))*sin(2*angle(n))+e(1,2)*cos(2*angle(n));
     %  e_prime(2,1,n) = e_prime(1,2,n)
   end

%AP=Q*A*Q';
%Display Segment

plot(n,e_prime(1,n))

%disp('Original Matrix')
%disp(e)
%disp('Rotation Matrix')
%disp(e_prime)
%disp('Transformed Matrix')
%disp(AP)