%MATLAB CODE C-1: Elasticity, Applications and Numerics
%Program to Calculate Components of a Second-Order Tensor (Matrix) under a
%rotation transformation
%Q = Rotational Tensor, A = Original Tensor to be Transformed, AP =
%Transformed Tensor clear
%Input Tensors (matrices) in MATLAB format
syms cos_theta
syms sin_theta
syms a11 a12 a22 a21
syms x
Q=[cos_theta,sin_theta;-sin_theta,cos_theta]
A=[0.002,0.001;0.001,-0.004] %strain tensor
AP= [0,0;0,0]
%Apply transformation law

acum= [x,x;x,x]
for i= 1:2
   for j=1:2
       
       for p = 1:2
           for q=1:2
               acum(i,j) = acum(i,j) + Q(i,p)*Q(j,q)*A(p,q)
            %  AP(i,j)= Q(i,p)*Q(j,q)*A(p,q)
           end   
       end
    AP(i,j) = acum(i,j);
   end
end

%AP=Q*A*Q';
%Display Segment
disp('Original Matrix')
disp(A)
disp('Rotation Matrix')
disp(Q)
disp('Transformed Matrix')
disp(AP)