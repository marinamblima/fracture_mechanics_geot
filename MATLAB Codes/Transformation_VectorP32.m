%MATLAB CODE C-1: Elasticity, Applications and Numerics
%Program to Calculate Components of a Second-Order Tensor (Matrix) under a
%rotation transformation
%Q = Rotational Tensor, A = Original Tensor to be Transformed, AP =
%Transformed Tensor clear
%Input Tensors (matrices) in MATLAB format
%Sigma=[2,1,-4;1,4,0;-4,0,1]
Sigma=[4,1,0;1,-6,2;0,2,1]
angle=[0:0.314159265358979:3.14159265358979]
%n_generic=[cos_theta,sin_theta,0]

%Apply transformation law


for k= 1:11 %size of angle vector
    n = [cos(angle(k));sin(angle(k));0];
    for i=1:3 %size of traction vector
        acum = 0;
        for j = 1:3 %Multiplication of stress tensor and normal vector
           acum  = acum + Sigma(i,j)*n(j);
        end
        T(i) = acum;
    end
    
    T_magnitude (k) = sqrt(T(1)*T(1)+ T(2)*T(2)+ T(3)*T(3));
end

%AP=Q*A*Q';
%Display Segment
plot(angle,T_magnitude)