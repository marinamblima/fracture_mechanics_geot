%MATLAB CODE C-1: Elasticity, Applications and Numerics
%Program to Calculate Components of a Second-Order Tensor (Matrix) under a
%rotation transformation
%Q = Rotational Tensor, A = Original Tensor to be Transformed, AP =
%Transformed Tensor clear
%Input Tensors (matrices) in MATLAB format
syms cos_theta
syms sin_theta
syms b1
syms b2
syms x
Q=[cos_theta,sin_theta;-sin_theta,cos_theta]
B=[b1;b2]
BP= [b1;b1]
%Apply transformation law

acum=[x;x]


for i= 1:2
 %   M(i) = Q(i,1)*B(1)
    for j=1:2
      acum(i) = acum(i) + Q(i,j)*B(j)
    end
    BP(i) = acum(i);
end

%AP=Q*A*Q';
%Display Segment
disp('Original Matrix')
disp(B)
disp('Rotation Matrix')
disp(Q)
disp('Transformed Matrix')
disp(BP)