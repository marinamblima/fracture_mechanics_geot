%MATLAB CODE 2.10: Elasticity, Applications and Numerics
%syms cos_theta
a = [1,0,0;0,1,0;0,0,1];
b = [-1,1,0; 1,1,0; 0,0,1];



for n=1:3
    for m=1:3
%Q(n,m) = a(1,1)*b(1,1)+a(1,2)*b(1,2)+a(1,3)*b(1,3)/(norm(a)*norm(b))        
Q(n,m) = (a(n,1)*b(m,1)+a(n,2)*b(m,2)+a(n,3)*b(m,3))/(sqrt(a(n,1)^2+a(n,2)^2+a(n,3)^2)*sqrt(b(m,1)^2+b(m,2)^2+b(m,3)^2));
    end
end

Q